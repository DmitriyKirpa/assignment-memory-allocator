#include "mem_internals.h"
#include "mem.h"
#include <inttypes.h>
#include <stdio.h>

const uint8_t ALLOC_SIZE = 50;
const uint16_t ALLOC_BIG_SIZE = REGION_MIN_SIZE * 2;
const uint8_t ALLOC_VALUE = 55;

extern bool heap_inited;
int failure_num = 0;

void test_case(int test, char* success_message, char* fail_message) {
  if(test) printf("%s\n", success_message);
  else {
    printf("%s\n", fail_message);
    failure_num += 1;
  }
}

void* allocate_memory_and_return_pointer() {
  void* pointer = _malloc(ALLOC_SIZE);
  return pointer;
}

void* allocate_bigmemory_and_return_pointer() {
  void* pointer = _malloc(ALLOC_BIG_SIZE);
  return pointer;
}

void test_allocation() {
  printf("**********ALLOCATION TEST**********\n");
  uint8_t* memory = (uint8_t*) allocate_memory_and_return_pointer();
  *memory = ALLOC_VALUE;
  test_case(*memory == ALLOC_VALUE, "Allocation done", "Error: allocation failed");
  struct block_header* allocated = (struct block_header*) (memory - offsetof(struct block_header, contents));
  test_case(!allocated->is_free, "Block is booked", "Error: allocated block is marked as free");
  test_case((allocated->capacity).bytes >= ALLOC_SIZE, "All required space is allocated", "Not enough space is allocated");
  printf("Allocated: %d <-> Required: %d\n", (int)(allocated->capacity).bytes, ALLOC_SIZE);
  printf("***********************************\n\n");
}

void test_free_one() {
  printf("**************FREE ONE BLOCK TEST**************\n");
  uint8_t* memory = (uint8_t*) allocate_memory_and_return_pointer();
  *memory = ALLOC_VALUE;
  test_case(*memory == ALLOC_VALUE, "Allocation done", "Error: allocation failed");
  _free(memory);
  struct block_header* freed = (struct block_header*) (memory - offsetof(struct block_header, contents));
  test_case(freed->is_free, "Block is freed", "Error: allocated block is marked as busy");
  printf("New block size: %d (bloated to region size)\n", (int)(freed->capacity).bytes);
  printf("***********************************************\n\n");
}

void test_free_two() {
  printf("**************FREE TWO BLOCKS TEST**************\n");
  uint8_t* memory1 = (uint8_t*) allocate_memory_and_return_pointer();
  uint8_t* memory2 = (uint8_t*) allocate_memory_and_return_pointer();
  uint8_t* memory3 = (uint8_t*) allocate_memory_and_return_pointer();
  *memory1 = ALLOC_VALUE;
  *memory2 = ALLOC_VALUE;
  *memory3 = ALLOC_VALUE;
  test_case((*memory1 == ALLOC_VALUE) && (*memory2 == ALLOC_VALUE) && (*memory3 == ALLOC_VALUE),
    "Allocation done",
    "Error: allocation failed"
  );
  _free(memory2);
  struct block_header* freed2 = (struct block_header*) (memory2 - offsetof(struct block_header, contents));
  test_case(freed2->is_free, "Block2 is freed", "Error: allocated block is marked as busy");
  printf("New block size: %d (nothing changed, next is busy)\n", (int)(freed2->capacity).bytes);
  _free(memory1);
  struct block_header* freed1 = (struct block_header*) (memory1 - offsetof(struct block_header, contents));
  test_case(freed1->is_free, "Block1 is freed", "Error: allocated block is marked as busy");
  test_case((freed1->capacity).bytes >= ALLOC_SIZE * 2, "Two free blocks merged", "Error: blocks did not merge");
  printf("New block size: %d (merged with next)\n", (int)(freed1->capacity).bytes);
  printf("************************************************\n\n");
}

void test_giant_allocation() {
  printf("**********REGION NOT ENOUGH TEST**********\n");
  uint8_t* memory = (uint8_t*) allocate_bigmemory_and_return_pointer();
  *memory = ALLOC_VALUE;
  test_case(*memory == ALLOC_VALUE, "Allocation done", "Error: allocation failed");
  struct block_header* allocated = (struct block_header*) (memory - offsetof(struct block_header, contents));
  test_case(!allocated->is_free, "Block is booked", "Error: allocated block is marked as free");
  test_case((allocated->capacity).bytes >= REGION_MIN_SIZE, "All required space is allocated", "Not enough space is allocated");
  printf("Allocated: %d\n", (int)(allocated->capacity).bytes);
  printf("******************************************\n\n");
}

int main() {
  heap_inited = false;
  test_allocation();
  
  heap_inited = false;
  test_free_one();
  
  heap_inited = false;
  test_free_two();
  
  heap_inited = false;
  test_giant_allocation();
  
  printf("Number of failed cases: %d\n", failure_num);
  return 0;
}
